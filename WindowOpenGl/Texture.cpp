#include "Texture.h"

Texture::Texture(GLenum textureTarget, const std::string& fileName)
{

	m_textureTarget = textureTarget;
	m_fileName = fileName;
}

Texture::~Texture()
{
}

bool Texture::Load()
{
	//load the texture using SOIL lib
	m_texture = SOIL_load_OGL_texture(
		m_fileName.c_str(),
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
	);

	if (m_texture == 0)
	{
		//error message
		return false;
	}

	glBindTexture(m_textureTarget, m_texture);

	return true;
}

void Texture::Bind(GLenum textureUnit, GLuint textureShaderRef)
{
	//texture Unit 0
	glActiveTexture(textureUnit);
	glBindTexture(m_textureTarget, m_texture);

	//send the texture to the shader
	glUniform1i(textureShaderRef, 0);
}