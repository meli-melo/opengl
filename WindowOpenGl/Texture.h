#ifndef DEF_TEXTURE
#define DEF_TEXTURE

#pragma once

#include <string>
#include <GL/glew.h>
#include <SOIL2/SOIL2.h>

class Texture
{
public:
	Texture(GLenum textureTarget, const std::string& fileName);
	~Texture();

	bool Load();
	void Bind(GLenum TextureUnit, GLuint textureShaderRef);

private:
	std::string m_fileName;
	GLenum m_textureTarget;
	GLuint m_texture;
};

#endif