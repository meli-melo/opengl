#ifndef DEF_CUBE
#define DEF_CUBE

#include "pch.h"
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "shader.hpp"
#include <string>
#include "objLoader.h"
#include <vector>


#pragma once
class Cube
{
private:
	GLuint m_vertexBuffer;
	GLuint m_colorBuffer;
	GLuint m_uvBuffer;
	GLuint m_shaderPrg;
	GLuint m_texture;

	GLuint m_textureId;
	GLuint m_matrixId;
	float m_rotationSpeed;
	GLuint loadDDS(const char * imagepath);

public:
	Cube(float size, std::string const vertexShader, std::string const fragmentShader, std::string texturePath, std::string modelPath);
	~Cube();
	void display(glm::mat4 &MVP);
	void cleanup();
};
#endif
