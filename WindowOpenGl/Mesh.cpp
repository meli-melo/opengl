#include "pch.h"
#include "Mesh.h"

Mesh::MeshEntry::MeshEntry()
{
	m_vb = GL_INVALID_VALUE;
	m_ib = GL_INVALID_VALUE;
	m_numIndices = 0;
	m_materialIndex = INVALID_MATERIAL;
}

Mesh::MeshEntry::~MeshEntry()
{
	if (m_vb != GL_INVALID_VALUE)
	{
		glDeleteBuffers(1, &m_vb);
	}

	if (m_ib != GL_INVALID_VALUE)
	{
		glDeleteBuffers(1, &m_ib);
	}
}

bool Mesh::MeshEntry::Init(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices)
{
	m_numIndices = indices.size();

	glGenBuffers(1, &m_vb);
	glBindBuffer(GL_ARRAY_BUFFER, m_vb);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*vertices.size(), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &m_ib);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ib);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*m_numIndices, &indices[0], GL_STATIC_DRAW);

	return true;
}

void Mesh::Clear()
{
	for (unsigned int i = 0; i < m_textures.size(); i++)
	{
		SAFE_DELETE(m_textures[i]);
	}
}

bool Mesh::LoadMesh(const std::string& filename)
{
	//clear the existing mesh
	Clear();

	bool res = false;

	Assimp::Importer importer;

	const aiScene* pScene = importer.ReadFile(filename.c_str(),
											aiProcess_Triangulate |
											aiProcess_GenSmoothNormals |
											aiProcess_FlipUVs);

	if (pScene)
	{
		res = InitFromScene(pScene, filename);
	}
	else
	{
		printf("Error parsing '%s'\n", filename.c_str(), importer.GetErrorString());
	}

	return res;
}

bool Mesh::InitFromScene(const aiScene* pScene, const std::string& filename)
{
	m_entries.resize(pScene->mNumMeshes);
	printf("number of entries %d\n", pScene->mNumMeshes);
	m_textures.resize(pScene->mNumMaterials);
	printf("number of entries %d\n", pScene->mNumMaterials);

	for (unsigned int i = 0; i < m_entries.size(); i++)
	{
		const aiMesh* pMesh = pScene->mMeshes[i];
		InitMesh(i, pMesh);
	}

	return InitMaterials(pScene, filename);
}

void Mesh::InitMesh(unsigned int index, const aiMesh* pMesh)
{
	m_entries[index].m_materialIndex = pMesh->mMaterialIndex;

	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;

	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);
	for (unsigned int i = 0; i < pMesh->mNumVertices; i++)
	{
		const aiVector3D* pPos = &(pMesh->mVertices[i]);
		const aiVector3D* pNormal = &(pMesh->mNormals[i]);
		const aiVector3D* pTexCoord = pMesh->HasTextureCoords(0) ? &(pMesh->mTextureCoords[0][i]) : &Zero3D;

		Vertex v(	glm::vec3(pPos->x, pPos->y, pPos->z),
					glm::vec2(pTexCoord->x, pTexCoord->y),
					glm::vec3(pNormal->x, pNormal->y, pNormal->z)
				);

		//printf("posx %f posy %f posz %f", pPos->x, pPos->y, pPos->z);
		vertices.push_back(v);
	}

	for (unsigned int i = 0; i < pMesh->mNumFaces; i++)
	{
		const aiFace& face = pMesh->mFaces[i];
		assert(face.mNumIndices == 3);
		indices.push_back(face.mIndices[0]);
		indices.push_back(face.mIndices[1]);
		indices.push_back(face.mIndices[2]);
		//printf("Face %d %d %d %d\n", i, face.mIndices[0], face.mIndices[1], face.mIndices[2]);
	}

	m_entries[index].Init(vertices, indices);
}

bool Mesh::InitMaterials(const aiScene* pScene, const std::string& filename)
{
	std::string::size_type slashIndex = filename.find_last_of("/");
	std::string dir;

	if (slashIndex == std::string::npos)
	{
		dir = ".";
	}
	else if (slashIndex == 0)
	{
		dir = "/";
	}
	else
	{
		dir = filename.substr(0, slashIndex);
	}

	bool ret = true;

	for (unsigned int i = 0; i < pScene->mNumMaterials; i++)
	{
		const aiMaterial* pMaterial = pScene->mMaterials[i];
		m_textures[i] = NULL;

		int test = pMaterial->GetTextureCount(aiTextureType_DIFFUSE);
		printf("texture count %d\n", test);
		if (pMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0)
		{
			aiString path;
			
			if (pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
			{
				std::string fullPath = dir + "/" + path.data;
				printf("path to texture %s \n", fullPath);
				m_textures[i] = new Texture(GL_TEXTURE_2D, fullPath.c_str());

				if (!m_textures[i]->Load())
				{
					printf("Error loading texture '%s'\n", fullPath.c_str());
					delete m_textures[i];
					m_textures[i] = NULL;
					ret = false;
				}
				else
				{
					printf("Texture loaded\n");
				}
			}
		}

		if (!m_textures[i])
		{
			printf("default texture\n");
			m_textures[i] = new Texture(GL_TEXTURE_2D, "../Content/white.png");
			ret = m_textures[i]->Load();
		}
	}

	return ret;
}

void Mesh::Render(glm::mat4 &MVP)
{
	glUseProgram(m_shaderPrg);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glUniformMatrix4fv(m_matrixId, 1, GL_FALSE, &MVP[0][0]);

	for (unsigned int i = 0; i < m_entries.size(); i++)
	{
		//vertices
		glBindBuffer(GL_ARRAY_BUFFER, m_entries[i].m_vb);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

		//uvs
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (const GLvoid*)0);
		//glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		//normals
		//glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)20);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_entries[i].m_ib);

		const unsigned int materialIndex = m_entries[i].m_materialIndex;
		if (materialIndex < m_textures.size() && m_textures[materialIndex])
		{
			m_textures[materialIndex]->Bind(GL_TEXTURE0, m_textureId);
		}

		glDrawElements(GL_TRIANGLES, m_entries[i].m_numIndices, GL_UNSIGNED_INT, 0);
	}

	//glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
}

Mesh::Mesh(std::string const vertexShader, std::string const fragmenShader)
{
	m_shaderPrg = LoadShaders(vertexShader.c_str(), fragmenShader.c_str());

	m_matrixId = glGetUniformLocation(m_shaderPrg, "MVP");

	m_textureId = glGetUniformLocation(m_shaderPrg, "myTextureSampler");
}

Mesh::~Mesh()
{
	Clear();
}

void Mesh::CleanUp()
{
	//Clean up VBO and shader
	glDeleteProgram(m_shaderPrg);
}