#ifndef DEF_MESH
#define DEF_MESH
#pragma once

#include "pch.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "objLoader.h"
#include "SOIL2/SOIL2.h"

#include "shader.hpp"
#include "Texture.h"
#include "utils.h"

struct Vertex
{
	glm::vec3 m_pos;
	glm::vec2 m_uv;
	glm::vec3 m_normal;

	Vertex() {}

	Vertex(const glm::vec3& pos, const glm::vec2& uv, const glm::vec3& normal)
	{
		m_pos = pos;
		m_uv = uv;
		m_normal = normal;
	}
};

class Mesh
{
private:
	//GLuint m_vertexBuffer;
	//GLuint m_colorBuffer;
	//GLuint m_uvBuffer;
	GLuint m_shaderPrg;
	//GLuint m_texture;

	GLuint m_textureId;
	GLuint m_matrixId;
	//float m_rotationSpeed;
	//int m_nbTriangles;


	void Clear();
	bool InitFromScene(const aiScene* pScene, const std::string& filename);
	void InitMesh(unsigned int index, const aiMesh* pMesh);
	bool InitMaterials(const aiScene* pScene, const std::string& filename);

#define INVALID_MATERIAL 0xFFFFFFFF

	struct MeshEntry
	{
		MeshEntry();

		~MeshEntry();

		bool Init(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices);

		GLuint m_vb;
		GLuint m_uvb;
		GLuint m_ib;
		unsigned int m_numIndices;
		unsigned int m_materialIndex;
	};

	std::vector<MeshEntry> m_entries;
	std::vector<Texture*> m_textures;

public:
	//Mesh(std::string const vertexShader, std::string const fragmentShader, std::string const texturePath, std::string const modelPath);
	Mesh(std::string const vertexShader, std::string const fragmenShader);
	~Mesh();

	bool LoadMesh(const std::string& filename);
	void Render(glm::mat4 &MVP);

	//GLuint LoadDDS(const char * imagepath);
	//void Display(glm::mat4 &MVP);
	void CleanUp();
};

#endif