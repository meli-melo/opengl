#include "pch.h"
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
//#include <glm/gtx/transform.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "shader.hpp"
#include "Cube.h"
#include "Triangle.h"
#include "Controls.h"
#include "Mesh.h"

#include <assimp/Importer.hpp>
using namespace glm;

//GLuint loadBMP_custom(const char * imagepath);

void MainLoop(GLFWwindow* window)
{

	//Projection matrix : 45deg Field of view, 4:3 ratio, display range : 0.1 unit <--> 100 units
	glm::mat4 Projection = glm::perspective(glm::radians(45.0f), 16.0f / 9.0f, 0.1f, 100.0f);

	//Camera matrix
	glm::mat4 View = glm::lookAt(
		glm::vec3(4, 3, 3),	//Camera is at (4,3,3) in world space
		glm::vec3(0, 0, 0),	//and looks at the origin
		glm::vec3(0, 1, 0)	//head is up (set to 0, -1, 0 to look upside down)
	);

	glm::mat4 Model = glm::mat4(1.0f);
	glm::mat4 TriangleModel = glm::mat4(1.0f);
	TriangleModel = translate(TriangleModel, vec3(-1.0f, 1.0f, -2.0f));

	//Our ModelViewProjection : multiplication of our 3 matrices
	glm::mat4 MVP;	//remember, matrix multiplication is the other way around
	glm::mat4 TriMVP = Projection * View * TriangleModel;

	Mesh cubeTest("SimpleVertexShader.txt", "SimpleFragmentShader.txt");
	cubeTest.LoadMesh("CubeSphere.fbx");

	//Cube cube(2.0, "SimpleVertexShader.txt", "SimpleFragmentShader.txt", "uvtemplate.DDS", "cubUv.obj");
	//Triangle triangle(2.0, "SimpleVertexShader.txt", "SimpleFragmentShader.txt", "triangleTexture.dds");

	float angle(0.001);
	SetWindowRef(window);
	do
	{
		glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
		//Clear the screen. It is not mentioned before tutorial 2 but it can cause flickering so it is there nonetheless
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		computeMatricesFromInputs();
		glm::mat4 ProjectionMarix = getProjectionMatrix();
		glm::mat4 ViewMatrix = getViewMatrix();

		//Model = rotate(Model, angle, vec3(0, 1, 0));
		//MVP = ProjectionMarix * View * Model;
		MVP = ProjectionMarix * ViewMatrix * Model;
		cubeTest.Render(MVP);
		//cube.display(MVP);

		TriangleModel = rotate(TriangleModel, angle, vec3(0, 1, 0));
		//MVP = Projection * View * TriangleModel;
		MVP = ProjectionMarix * ViewMatrix * TriangleModel;
		//triangle.display(MVP);

		//swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
	}//check if the ESC key was pressed or the window was closed
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0);

	cubeTest.CleanUp();
	//cube.cleanup();
	//triangle.cleanup();
}

int main()
{
	//Initialize GLFW
	glewExperimental = true; //Needed for core profile
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}


	glfwWindowHint(GLFW_SAMPLES, 4); //x4 antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL 

	//Open a window and create its OpenGl context
	GLFWwindow* window; //(in the accompanying source code, this variable is global for simplicity)
	window = glfwCreateWindow(1024, 576, "test", NULL, NULL);
	if (window == NULL)
	{
		fprintf(stderr, "Failed to open GLFW window. If you have an intel GPU, they are 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window); //Initialize GLEW
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	//Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	//Hide the mouse and enable unlimited movement
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	//Set the mouse at the center of the screen
	glfwPollEvents();
	glfwSetCursorPos(window, 1024 / 2, 576 / 2);

	//Enable depth test
	glEnable(GL_DEPTH_TEST);
	//Accept fragment if it is closer to the camera than the former one
	glDepthFunc(GL_LESS);

	//Cull triangles which normal is not towards the camera
	//glEnable(GL_CULL_FACE);

	//VAO is done once the window is created and before any other ÔepnGL call
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	MainLoop(window);

	glDeleteVertexArrays(1, &VertexArrayID);
	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

GLuint loadBMP_custom(const char * imagepath)
{
	//data read from the header of the BMP file
	unsigned char header[54];	//each BMP file begins by a 54-bytes header
	unsigned int dataPos;	//Position in the file where the actual data begins
	unsigned int width, height;
	unsigned int imageSize;	//=width*height*3
	//Actual RGB data
	unsigned char * data;

	//Open the file
	FILE * file = fopen(imagepath, "rb");
	if (!file)
	{
		printf("Image could not be opened\n");
		return 0;
	}

	if (fread(header, 1, 54, file) != 54)
	{
		//if not 54 bytes read : problem
		printf("Not a correct BMP file \n");
		return false;
	}

	if (header[0] != 'B' || header[1] != 'M')
	{
		printf("Not a correct BMP file\n");
		return 0;
	}

	//read ints from the byte array
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	//Some BMP files are misformatted, guess missing information
	if (imageSize == 0)
		imageSize = width * height * 3;	//3: one byte for each red, Green and Blue component
	if (dataPos == 0)
		dataPos = 54; //The BMP header is done that way

	//Create a buffer
	data = new unsigned char[imageSize];

	//Read the actual data from the file into the buffer
	fread(data, 1, imageSize, file);

	//Everything is in memory now, the filee can be closed
	fclose(file);

	//Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	//"Bind" the newly created texture : all future texture functions will modify this texure
	glBindTexture(GL_TEXTURE_2D, textureID);

	//Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0,  GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	//Wehn MAGnifying the image (no bigger mipmapvailable), use LINEAR filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//When MINifying the image, use a LINEAR blenf of two mipmaps, each filtered LINEARLY too
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	//Generate mipmaps, by the way
	glGenerateMipmap(GL_TEXTURE_2D);

	return 0;
}

#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII

GLuint loadDDS(const char * imagepath)
{

	unsigned char header[124];

	FILE *fp;

	/* try to open the file */
	fp = fopen(imagepath, "rb");
	if (fp == NULL) {
		printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath); getchar();
		return 0;
	}

	/* verify the type of file */
	char filecode[4];
	fread(filecode, 1, 4, fp);
	if (strncmp(filecode, "DDS ", 4) != 0) {
		fclose(fp);
		return 0;
	}

	/* get the surface desc */
	fread(&header, 124, 1, fp);

	unsigned int height = *(unsigned int*)&(header[8]);
	unsigned int width = *(unsigned int*)&(header[12]);
	unsigned int linearSize = *(unsigned int*)&(header[16]);
	unsigned int mipMapCount = *(unsigned int*)&(header[24]);
	unsigned int fourCC = *(unsigned int*)&(header[80]);


	unsigned char * buffer;
	unsigned int bufsize;
	/* how big is it going to be including all mipmaps? */
	bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize;
	buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char));
	fread(buffer, 1, bufsize, fp);
	/* close the file pointer */
	fclose(fp);

	unsigned int components = (fourCC == FOURCC_DXT1) ? 3 : 4;
	unsigned int format;
	switch (fourCC)
	{
	case FOURCC_DXT1:
		format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
		break;
	case FOURCC_DXT3:
		format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
		break;
	case FOURCC_DXT5:
		format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
		break;
	default:
		free(buffer);
		return 0;
	}

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
	unsigned int offset = 0;

	/* load the mipmaps */
	for (unsigned int level = 0; level < mipMapCount && (width || height); ++level)
	{
		unsigned int size = ((width + 3) / 4)*((height + 3) / 4)*blockSize;
		glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,
			0, size, buffer + offset);

		offset += size;
		width /= 2;
		height /= 2;

		// Deal with Non-Power-Of-Two textures. This code is not included in the webpage to reduce clutter.
		if (width < 1) width = 1;
		if (height < 1) height = 1;

	}

	free(buffer);

	return textureID;
}