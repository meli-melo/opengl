#ifndef CONTROLS_HPP
#define CONTROLS_HPP

void computeMatricesFromInputs();
glm::mat4 getProjectionMatrix();
glm::mat4 getViewMatrix();
void SetWindowRef(GLFWwindow* windowRef);

#endif