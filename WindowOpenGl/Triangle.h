#ifndef DEF_TRIANGLE
#define DEF_TRIANGLE

#include "pch.h"
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "shader.hpp"
#include <string>

#pragma once
class Triangle
{
private:
	GLuint m_vertexBuffer;
	GLuint m_colorBuffer;
	GLuint m_uvBuffer;
	GLuint m_shaderPrg;
	GLuint m_texture;

	GLuint m_textureId;
	GLuint m_matrixId;
	float m_rotationSpeed;
	GLuint loadDDS(const char * imagepath);

public:
	Triangle(float size, std::string const vertexShader, std::string const fragmentShader, std::string texturePath);
	~Triangle();
	void display(glm::mat4 &MVP);
	void cleanup();
};

#endif