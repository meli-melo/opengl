#ifndef DEF_UTILS
#define DEF_UTILS

#define SAFE_DELETE(p) if(p) { delete p; p = NULL;}

#pragma once

#endif