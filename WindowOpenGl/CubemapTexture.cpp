#include "CubemapTexture.h"



CubemapTexture::CubemapTexture(const std::string& Directory,
								const std::string& PosXFilename,
								const std::string& NegXFilename,
								const std::string& PosYFilename,
								const std::string& NegYFilename,
								const std::string& PosZFilename,
								const std::string& NegZFilename)
{
	std::string::const_iterator it = Directory.end();
	it--;
	std::string baseDir = (*it == '/') ? Directory : Directory + "/";

	m_filename[0] = baseDir + PosXFilename;
	m_filename[1] = baseDir + NegXFilename;
	m_filename[2] = baseDir + PosYFilename;
	m_filename[3] = baseDir + NegYFilename;
	m_filename[4] = baseDir + PosZFilename;
	m_filename[5] = baseDir + NegZFilename;

	m_textureObj = 0;
}


CubemapTexture::~CubemapTexture()
{
	if (m_textureObj != 0)
		glDeleteTextures(1, &m_textureObj);
}

bool CubemapTexture::Load()
{
	glGenTextures(1, &m_textureObj);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_textureObj);

	/* load 6 images into a new OpenGL cube map, forcing RGB */
	m_textureObj = SOIL_load_OGL_cubemap
	(
		m_filename[0].c_str(),
		m_filename[1].c_str(),
		m_filename[2].c_str(),
		m_filename[3].c_str(),
		m_filename[4].c_str(),
		m_filename[5].c_str(),
		SOIL_LOAD_RGB,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS
	);

	glBindTexture(GL_TEXTURE_CUBE_MAP, m_textureObj);

	return true;
}
