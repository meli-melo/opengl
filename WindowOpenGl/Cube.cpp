#include "pch.h"
#include "Cube.h"
#include <iostream>

Cube::Cube(float size, std::string const vertexShader, std::string const fragmentShader, std::string const texturePath, std::string const modelPath)
{
	//create and compute our GLSL program from the shaders
	m_shaderPrg = LoadShaders(vertexShader.c_str(), fragmentShader.c_str());

	//get a handle for our "MVP" uniform
	//Only during the initialization
	m_matrixId = glGetUniformLocation(m_shaderPrg, "MVP");

	// Division of the size parameter
	size /= 2;

	// temp Vertices
	GLfloat verticesTmp[] = { -size, -size, -size,   size, -size, -size,   size, size, -size,     // Face 1
						   -size, -size, -size,   -size, size, -size,   size, size, -size,     // Face 1

						   size, -size, size,   size, -size, -size,   size, size, -size,       // Face 2
						   size, -size, size,   size, size, size,   size, size, -size,         // Face 2

						   -size, -size, size,   size, -size, size,   size, -size, -size,      // Face 3
						   -size, -size, size,   -size, -size, -size,   size, -size, -size,    // Face 3

						   -size, -size, size,   size, -size, size,   size, size, size,        // Face 4
						   -size, -size, size,   -size, size, size,   size, size, size,        // Face 4

						   -size, -size, -size,   -size, -size, size,   -size, size, size,     // Face 5
						   -size, -size, -size,   -size, size, -size,   -size, size, size,     // Face 5

						   -size, size, size,   size, size, size,   size, size, -size,         // Face 6
						   -size, size, size,   -size, size, -size,   size, size, -size };      // Face 6

	// temp colors
	GLfloat colorsTmp[] = { 1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,           // Face 1
						   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,           // Face 1

						   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,           // Face 2
						   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,           // Face 2

						   0.0, 0.0, 1.0,   0.0, 0.0, 1.0,   0.0, 0.0, 1.0,           // Face 3
						   0.0, 0.0, 1.0,   0.0, 0.0, 1.0,   0.0, 0.0, 1.0,           // Face 3

						   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,           // Face 4
						   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,           // Face 4

						   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,           // Face 5
						   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,           // Face 5

						   0.0, 0.0, 1.0,   0.0, 0.0, 1.0,   0.0, 0.0, 1.0,           // Face 6
						   0.0, 0.0, 1.0,   0.0, 0.0, 1.0,   0.0, 0.0, 1.0 };          // Face 6

	//Two UV coordinates for each vertex.They were created with Blender. Learn later how to do it
	GLfloat uvTmp[] = {	0.000059f, 1.0f - 0.000004f,
						0.000103f, 1.0f - 0.336048f,
						0.335973f, 1.0f - 0.335903f,
						1.000023f, 1.0f - 0.000013f,
						0.667979f, 1.0f - 0.335851f,
						0.999958f, 1.0f - 0.336064f,
						0.667979f, 1.0f - 0.335851f,
						0.336024f, 1.0f - 0.671877f,
						0.667969f, 1.0f - 0.671889f,
						1.000023f, 1.0f - 0.000013f,
						0.668104f, 1.0f - 0.000013f,
						0.667979f, 1.0f - 0.335851f,
						0.000059f, 1.0f - 0.000004f,
						0.335973f, 1.0f - 0.335903f,
						0.336098f, 1.0f - 0.000071f,
						0.667979f, 1.0f - 0.335851f,
						0.335973f, 1.0f - 0.335903f,
						0.336024f, 1.0f - 0.671877f,
						1.000004f, 1.0f - 0.671847f,
						0.999958f, 1.0f - 0.336064f,
						0.667979f, 1.0f - 0.335851f,
						0.668104f, 1.0f - 0.000013f,
						0.335973f, 1.0f - 0.335903f,
						0.667979f, 1.0f - 0.335851f,
						0.335973f, 1.0f - 0.335903f,
						0.668104f, 1.0f - 0.000013f,
						0.336098f, 1.0f - 0.000071f,
						0.000103f, 1.0f - 0.336048f,
						0.000004f, 1.0f - 0.671870f,
						0.336024f, 1.0f - 0.671877f,
						0.000103f, 1.0f - 0.336048f,
						0.336024f, 1.0f - 0.671877f,
						0.335973f, 1.0f - 0.335903f,
						0.667969f, 1.0f - 0.671889f,
						1.000004f, 1.0f - 0.671847f,
						0.667979f, 1.0f - 0.335851f };


#pragma region load from file
	//std::vector<glm::vec3> vertices;
	//std::vector<glm::vec2> uvs;
	//std::vector<glm::vec3> normals;	//won't be used at the moment.
	//bool success = loadObj(modelPath.c_str(), vertices, uvs, normals);

	////Load it into a VBO
	//glGenBuffers(1, &m_vertexBuffer);
	//glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	//glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	//glGenBuffers(1, &m_uvBuffer);
	//glBindBuffer(GL_ARRAY_BUFFER, m_uvBuffer);
	//glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

	//m_texture = loadDDS(texturePath.c_str());
	//m_textureId = glGetUniformLocation(m_shaderPrg, "myTextureSampler");
#pragma endregion load from file

#pragma region old VBO loading
	
	//Generate 1 buffer, put the resulting identifier in vertexBuffer
	glGenBuffers(1, &m_vertexBuffer);
	//The following commands will talk about our 'vertexBuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	//Give our vertices to OpenGL
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTmp), verticesTmp, GL_STATIC_DRAW);

	glGenBuffers(1, &m_colorBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorsTmp), colorsTmp, GL_STATIC_DRAW);

	glGenBuffers(1, &m_uvBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(uvTmp), uvTmp, GL_STATIC_DRAW);
	
#pragma endregion old VBO loading

	m_rotationSpeed = 0.001f;
}


Cube::~Cube()
{
}

void Cube::display(glm::mat4 &MVP)
{
	//use our shader
	glUseProgram(m_shaderPrg);
	//send our transformation to the currently bound shader, in the MVP uniform
	//This is done in the main loop since each model will have a different MVP matrix (At least for the M part)
	glUniformMatrix4fv(m_matrixId, 1, GL_FALSE, &MVP[0][0]);
	//Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texture);
	//Set our "myTextureSampler" sampler to use Texture Unit 0
	glUniform1i(m_textureId, 0);

	//1st attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glVertexAttribPointer(
		0,	//attribute 0. No particular reason for 0, but must match the layout in the shader
		3,	//size
		GL_FLOAT,	//type
		GL_FALSE,	//normalized ?
		0,	//stride
		(void*)0	//array buffer offset
	);

	//glEnableVertexAttribArray(1);
	//glBindBuffer(GL_ARRAY_BUFFER, m_colorBuffer);
	//glVertexAttribPointer(
	//	1,	//attribute1 1. No particular reason for 1 but must match the layout in the shader
	//	3,	//size
	//	GL_FLOAT,	//type
	//	GL_FALSE,	//normalized ?
	//	0,	//stride
	//	(void*)0	//array buffer offset
	//);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, m_uvBuffer);
	glVertexAttribPointer(
		1,	//attribute 1. No particular reason for 1 but must match the layout in the shader
		2,	//size
		GL_FLOAT,	//type
		GL_FALSE,	//normalized ?
		0,	//stride
		(void*)0	//array buffer offset
	);

	//draw the triangles !
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);	//starting from vertex0; 3 vertices total -> 1 triangle; 12 triangles -> 6 squares

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII

GLuint Cube::loadDDS(const char * imagepath)
{

	unsigned char header[124];

	FILE *fp;

	/* try to open the file */
	fp = fopen(imagepath, "rb");
	if (fp == NULL) {
		printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath); getchar();
		return 0;
	}

	/* verify the type of file */
	char filecode[4];
	fread(filecode, 1, 4, fp);
	if (strncmp(filecode, "DDS ", 4) != 0) {
		fclose(fp);
		return 0;
	}

	/* get the surface desc */
	fread(&header, 124, 1, fp);

	unsigned int height = *(unsigned int*)&(header[8]);
	unsigned int width = *(unsigned int*)&(header[12]);
	unsigned int linearSize = *(unsigned int*)&(header[16]);
	unsigned int mipMapCount = *(unsigned int*)&(header[24]);
	unsigned int fourCC = *(unsigned int*)&(header[80]);


	unsigned char * buffer;
	unsigned int bufsize;
	/* how big is it going to be including all mipmaps? */
	bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize;
	buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char));
	fread(buffer, 1, bufsize, fp);
	/* close the file pointer */
	fclose(fp);

	unsigned int components = (fourCC == FOURCC_DXT1) ? 3 : 4;
	unsigned int format;
	switch (fourCC)
	{
	case FOURCC_DXT1:
		format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
		break;
	case FOURCC_DXT3:
		format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
		break;
	case FOURCC_DXT5:
		format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
		break;
	default:
		free(buffer);
		return 0;
	}

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
	unsigned int offset = 0;

	/* load the mipmaps */
	for (unsigned int level = 0; level < mipMapCount && (width || height); ++level)
	{
		unsigned int size = ((width + 3) / 4)*((height + 3) / 4)*blockSize;
		glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,
			0, size, buffer + offset);

		offset += size;
		width /= 2;
		height /= 2;

		// Deal with Non-Power-Of-Two textures. This code is not included in the webpage to reduce clutter.
		if (width < 1) width = 1;
		if (height < 1) height = 1;

	}

	free(buffer);

	return textureID;
}

void Cube::cleanup()
{
	// Cleanup VBO and shader
	glDeleteBuffers(1, &m_vertexBuffer);
	glDeleteBuffers(1, &m_colorBuffer);
	glDeleteBuffers(1, &m_uvBuffer);
	glDeleteProgram(m_shaderPrg);
	glDeleteTextures(1, &m_texture);
}
