#ifndef DEF_CUBEMAP
#define DEF_CUBEMAP

#pragma once
#include <string>
#include <GL/glew.h>
#include <SOIL2/SOIL2.h>

class CubemapTexture
{
public:
	CubemapTexture(const std::string& Directory,
			const std::string& PosXFilename,
			const std::string& NegXFilename,
			const std::string& PosYFilename,
			const std::string& NegYFilename,
			const std::string& PosZFilename,
			const std::string& NegZFilename);
	~CubemapTexture();
	bool Load();
	void Bind(GLenum TextureUnit);

private:
	std::string m_filename[6];
	GLuint m_textureObj;
};

#endif